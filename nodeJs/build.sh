#!/bin/bash

file="./project.properties"

while IFS='=' read -r key value
do
    key=$(echo $key | tr '.' '_')
    echo ${value}
    eval ${key}=${value}
done < "$file"

echo "app.repo" ${app_repo}
echo "app Repo name" ${app_repo_name}
echo "app Repo token" ${app_repo_token}
echo "app Repo id" ${app_repo_id}
echo "app Name " ${app_name}

echo "git clone coba"

rm -rf samplenodejs
git clone ${app_repo}
cd ${app_name}
# git checkout main
# git pull

ls -a
# node .
# npm install -g .